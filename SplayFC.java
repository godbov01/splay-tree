import java.util.Iterator;

/*
This file contains an implementation of a splay tree with fast cloning. That is, it's a splay tree with immutable cells.

The text below is the feedback from the automated judge that gave us a mark for correctness.
Performance was measured by the number of calls to key() and the number of cells created.
Our performance exactly matched the ideal performance of the judge solution.

Correctness was measured by checking the text output and diff-ing it with the judge output. Similar to the ACM-ICPC.
According to the judge output, none of our methods produced wrong output for all judge input.

Almost all output was exactly the same as the judge output. 
We were not penalised for producing equivalent output for the four updatingIterator methods at the bottom. 

- Varun Godbole
*/
/*

Judge feedback:
CONGRATS!!

Exact: 3053    Equivalent: 0        Wrong: 0      splay           key(): 22569 vs 22569          cells: 12005 vs 12005
Exact: 3035    Equivalent: 0        Wrong: 0      add             key(): 26571 vs 26571          cells: 14900 vs 14900
Exact: 3056    Equivalent: 0        Wrong: 0      contains        key(): 25379 vs 25379          cells: 11852 vs 11852
Exact: 3056    Equivalent: 0        Wrong: 0      headSet         key(): 26379 vs 26379          cells: 12763 vs 12763
Exact: 3051    Equivalent: 0        Wrong: 0      tailSet         key(): 27722 vs 27722          cells: 14185 vs 14185
Exact: 385     Equivalent: 0        Wrong: 0      subSet          key():  5439 vs 5439           cells:  2582 vs 2582
Exact: 3058    Equivalent: 0        Wrong: 0      remove          key(): 30790 vs 30790          cells: 14957 vs 14957
Exact: 261     Equivalent: 0        Wrong: 0      clone           key():     0 vs 0              cells:     0 vs 0

Exact:  1271   Equivalent:     0    Wrong:     0   snapHasNext
Exact:   500   Equivalent:     0    Wrong:     0   snapNext
Exact:   103   Equivalent:     0    Wrong:     0   snapRemove
Exact:   247   Equivalent:     0    Wrong:     0   snapShotIterator
      key():  3778 vs 3283       cells:  2047 vs 1305

Exact:  2940   Equivalent:    79    Wrong:     0   updatingHasNext
Exact:  1276   Equivalent:    31    Wrong:     0   updatingNext
Exact:   253   Equivalent:    41    Wrong:     0   updatingRemove
Exact:   247   Equivalent:     0    Wrong:     0   updatingIterator
      key(): 93163 vs 77405      cells: 48871 vs 40073

Totals:
  Exact: 26879    Equivalent: 156     Wrong: 0       key(): 276535 vs 260258      cells: 141609 vs 132054
 */


/**
 * An implementation of a splay tree of strings with fast cloning and subset extraction.
 * @author Varun Godbole 20742098 Sean Peters 20672528
 * @version 2.1
 */
public class SplayFC implements ISplayFC {

    /**
     * Stores the current root cell.
     */
    public Cell top;

    /**
     * Strings that are below (STRING_MIN) and above (STRING_MAX) all the words
     * that will be in a SplayFC tree.  Splaying on these is sometimes useful.
     */
    final static String STRING_MIN = "";

    final static String STRING_MAX = Character.toString(Character.MAX_VALUE);

    /**
     * Necessary for ConcurrentModificationException. 
     */
    int removeCount;

    /**
     * Constructs an empty SplayFC
     */
    public SplayFC() { 
        top = null;
    }

    /**
     * Constructs a SplayFC with c as the top
     * @param c - A Cell object. If null, the tree is empty.
     */
    public SplayFC(Cell c) { 
        top = c;
    }


    public Cell getTop() {
        return top;
    }


    public void setTop(Cell c) { 
        top = c;
    }

    /*
     * See ISplayFC.  You must not modify this method.
     */
    @Override
    public String toString() {
            if(top == null) return "[]\n";
            return top.toString("","   ", "   ",  " -");
    }

    /**
     * A small abbreviation for the Cell constructor, which is handy when you need to
     * construct cells in many different places in your code.
     * @param k The key for the new cell.
     * @param l The left child of the new cell.
     * @param r the right child of the new cell.
     * @return The newly constructed cell.
     */
    private static Cell cell(String k, Cell l, Cell r){
        return new Cell(k, l, r);
    }

    /**
     * Splay the tree with root c to build a new tree that has the same keys but has a node that is "nearest" to k at the root.
     * Here "nearest" means the new root node's key is equal to k if k is in the tree,
     * otherwise there must be no other key in the tree in between k and the root node's key.
     * (In the latter case there may be two "nearest" nodes - one less than k and one greater than k).
     * Splaying is done by returning a new tree rather than modifying the input tree. The returned tree
     * should generally share some of the existing cells from the input tree, and should create new cells for the remainder.
     * The exact scheme for splaying that must be implemented is described in the project specification.
     *
     * @param c A valid Cell object.
     * @param k A valid String
     * @return The root cell of the new tree.
     * @throws NullPointerException Thrown when c is null
     * @throws IllegalArgumentException Thrown when k is null
     */
    public Cell splay(Cell c, String k) throws NullPointerException, IllegalArgumentException{
            if(c == null){
                throw new NullPointerException("splay requires a non-null Cell");
                //return null;
            }
            
            if(k == null){
                throw new IllegalArgumentException("string is null");
            }

            // To help get you started, the code below shows how to implement
            // the cases for a left "zig-zig" step and a left "zig" step.  You need to
            // add the case for a left "zig-zag" step, and the cases for all three
            // kinds of right step (which mirror the left cases).

            int compareToCKey = k.compareTo(c.key());


            // Search left
            //k < c 
            if(compareToCKey < 0 && c.lt != null) {                                     
                int compareToLtKey = k.compareTo(c.lt.key());
                
                // left zig-zig step
                //k < c.lt
                if(compareToLtKey < 0 && c.lt.lt!=null) {                           
                    // Search recursively
                    Cell ll = splay(c.lt.lt, k);                
                    // Rearrange
                    Cell newRR = cell(c.key(), c.lt.rt, c.rt);  
                    return cell(ll.key(), ll.lt, cell(c.lt.key(), ll.rt, newRR));
                } 
                // left zig-zag step
                //k > c.lt
                else if(compareToLtKey > 0 && c.lt.rt != null) {    
                    Cell lr = splay(c.lt.rt, k);
                    Cell newL = cell(c.lt.key(), c.lt.lt, lr.lt);
                    return cell(lr.key(), newL, cell(c.key(), lr.rt, c.rt));
                }
                else
                    // left zig step
                    return cell(c.lt.key(), c.lt.lt, cell(c.key(),c.lt.rt,c.rt));
            }

            // Search right
            //c < k
            else if(compareToCKey > 0 && c.rt != null) {                   
                int compareToRtKey = k.compareTo(c.rt.key());
                
                //k > c.rt
                //right zig-zig step
                if(compareToRtKey > 0 && c.rt.rt != null) {
                    Cell rr = splay(c.rt.rt, k);
                    Cell newLL = cell(c.key(), c.lt, c.rt.lt);
                    return cell(rr.key(), cell(c.rt.key(), newLL, rr.lt), rr.rt);       
                }

                // right zig-zag step
                //k < c.lt
                else if(compareToRtKey < 0 && c.rt.lt != null) {    
                    Cell rl = splay(c.rt.lt, k);
                    Cell newR = cell(c.rt.key(), rl.rt, c.rt.rt);
                    return cell(rl.key(), cell(c.key(), c.lt, rl.lt), newR);
                }
                else
                    // right zig step
                    return cell(c.rt.key(), cell(c.key(), c.lt, c.rt.lt), c.rt.rt);
            }
            else
                return c;  // Special cases 1 and 2 in the specification
    }

    /**
     *
     * Insert a specified string key into the splay tree. If the tree is empty, create a new cell.
     * Otherwise, splay on k and if k is not found replace the resulting root cell, with key h,
     * by two new cells such that k is the new root and h is appropriately in either the left or right child.
     * 
     * @param k The string key to insert. 
     * @return True if k was not already in the splay tree.
     * @throws IllegalArgumentException If k is null.
     */
    public boolean add(String k) throws IllegalArgumentException{
        if(k == null){
            throw new IllegalArgumentException("Cannot add a null reference to the tree");
        }

        if(getTop() == null){
            setTop(cell(k, null, null));
            return true;
        }

        Cell h = splay(getTop(), k);
        int cmp = k.compareTo(h.key());
        
        if(cmp == 0){
            setTop(h);
            return false;
        }
        else if(cmp < 0)
        {
            Cell newRight = cell(h.key(), null, h.rt);
            setTop(cell(k, h.lt, newRight));
        }
        else if(cmp > 0){
            Cell newLeft = cell(h.key(), h.lt, null);
            setTop(cell(k, newLeft, h.rt));
        }

        return true;
    }

    /**
     * Remove a specified string from the splay tree.
     * This should be done by splaying on k and then splaying on the resulting left subtree to bring its maximum element to its root.
     * This is then rearranged along with the right subtree.
     * There are special cases for when the key is not found and when particular trees are null - you'll need to figure these out.
     * 
     * @param k A valid string to remove.
     * @return True if k was in the splay tree.
     * @throws IllegalArgumentException
     */
    public boolean remove(String k) throws IllegalArgumentException {
        ++removeCount;
        //false if tree is empty
        if(getTop() == null){
            return false;
        }

        //splay on k which brings it to the root.
        Cell toRemove = splay(getTop(), k);

        //if k was not in tree return false
        if(!k.equals(toRemove.key())){
            setTop(toRemove);
            return false;
        }

        if(toRemove.lt == null && toRemove.rt == null){
            setTop(null);
        }
        else if(toRemove.lt == null) {
            setTop(toRemove.rt);  
        }
        else {
            Cell newTop = splay(toRemove.lt, STRING_MAX);
            setTop(cell(newTop.key(), newTop.lt, toRemove.rt));
        }
        return true;
    }

    /**
     * Check whether a string k appears as a key in the splay tree.
     * This should be done via splaying, with the tree being updated to the result of splaying regardless of whether the key is found.
     * 
     * @param k A valid string to look for
     * @return True if k is in the tree.
     * @throws IllegalArgumentException If k is null.
     */
    public boolean contains(String k) throws IllegalArgumentException{
        if(getTop() == null)
            return false;

        setTop(splay(getTop(), k));

        return k.equals(getTop().key());
    }


    /**
     * Extract a splay tree that contains all keys in the current tree that are strictly less than k.
     * This should be done via splaying on k, updating this SplayFC tree, as well as returning an extracted tree with as much sharing of cells as possible.
     *
     * @param k The upper bound
     * @return The extracted splay tree.
     */
    public SplayFC headSet(String k) {
        if(getTop() == null){
            return new SplayFC(null);
        }
        
        Cell newTop = splay(getTop(), k);
        setTop(newTop);

        //newTop < k
        if(newTop.key().compareTo(k) < 0){
            return new SplayFC( cell(newTop.key(), newTop.lt, null));
        }
        //newTop >= k
        else{
            return new SplayFC(newTop.lt);
        }
    }

    /**
     * Extract a splay tree that contains all keys in the current tree that are greater than or equal to k.
     * This should be done via splaying on k, updating this SplayFC tree, as well as returning an extracted tree with as much sharing of cells as possible.
     *
     * @param k The string below which keys should be included.
     * @return The extacted splay tree.
     */
    public SplayFC tailSet(String k) {
        if(getTop() == null){
            return new SplayFC(null);
        }

        Cell newTop = splay(getTop(), k);
        setTop(newTop);

        //newTop >= k
        if(newTop.key().compareTo(k) >= 0){
            return new SplayFC(cell(newTop.key(), null, newTop.rt));
        }
        //newTop < k
        else{
            return new SplayFC(newTop.rt);
        }
    }

    /**
     * Extract a splay tree that contains all keys in the current tree that are greater than or equal to k1 and strictly less than k2.
     *
     * @param k1 The minimum string key to include.
     * @param k2 The string below which keys should be included. 
     * @return The extracted splay tree.
     */
    public SplayFC subSet(String k1, String k2) {
        return headSet(k2).tailSet(k1);
    }

    /**
     * Create a new SplayFC object that contains the same strings as this object.
     * This method must be fast. (SplayFC = Splay Tree with Fast Cloning)
     * In particular, cells should be shared with the original SplayFC object rather than being copied.
     * @return The created SplayFC object.
     */
    @Override
    public SplayFC clone(){        
        //cells should be shared.
        //cells are immutable.
        //therefore simply return the root.
        return new SplayFC(getTop());

    }

    /**
     * Create an iterator that visits the strings in this SplayFC tree in order.
     * If the tree is updated while the iterator is active, the iterator should still
     * visit the strings that were in the tree when the iterator was created.
     * The iterator's remove method should always throw UnsupportedOperationException.
     * The use of this iterator should not result in rebalancing of the original SplayFC tree.
     * @return The iterator object.
     */
    public Iterator<String> snapShotIterator(){
        return new SnapShotIterator(clone());
    }


    /**
     * Create an iterator that visits the strings in this SplayFC tree in order.
     * If the tree is updated while the iterator is active, the iterator should be affected by the update.
     * Specifically, each call to next should visit the least element currently in the tree that is greater
     * than the previously returned elements, and the hasNext method should be consistent with this.
     * The iterator's remove operation should be implemented.
     * The iterator's next method should throw java.util.ConcurrentModificationException if remove has been called on
     * the SplayFC tree since the most recent call to hasNext.
     * The use of this iterator should not result in rebalancing of the original SplayFC tree.
     * @return The iterator object.
     */
    public Iterator<String> updatingIterator() {
        return new UpdatingIterator();
    }

    /**
     * Internal class for the SnapShotIterator
     */
    public class SnapShotIterator implements Iterator<String> {
        /**
         * This cell is the current cell visited by the iterator.
         */
        Cell current;

        /**
         * Create a snapshot iterator and place it at a before first position.
         *
         * @param c A valid SplayFC object
         */
        public SnapShotIterator(SplayFC c){
            if (c.getTop() == null) {
                current = cell(null, null, null);
                return;
            }
            current = splay(c.getTop(), STRING_MIN);
            current = cell(null, null, current);
        }

        /**
         * Returns true if the iteration has more elements.
         * (In other words, returns true if next would return an element rather than throwing an exception.)
         * @return true if the iterator has more elements.
         */
        public boolean hasNext() {
            return current.rt != null;
        }

        /**
         * Returns the next element in the iteration.
         *
         * @return the next String in the iteration.
         * @throws java.util.NoSuchElementException iteration has no more elements.
         */
        public String next() throws java.util.NoSuchElementException{
            if(hasNext()){
                current = current.rt;
                String ret = current.key();

                Cell newRight;
                if (current.rt == null)
                {
                    newRight = null;
                }
                else {
                    newRight = splay(current.rt, STRING_MIN);
                }

                current = cell(current.key(), null, newRight);
                return ret;
            }
            else
                throw new java.util.NoSuchElementException("No more elements left");
        }

        /**
         * Unsupported method.
         */
        public void remove(){ 
            throw new UnsupportedOperationException("remove");
        }

        /**
         * Convert the structure of the current tree to a string.
         * @return String containing structure of tree.
         */
        @Override
        public String toString(){
            SplayFC tree = new SplayFC(current.rt);
            return tree.toString();
        }
    }

    /**
     * The UpdatingIterator for SplayFC
     */
    public class UpdatingIterator implements Iterator<String> {

        /**
         * Current key visited by iterator.
         */
        public String current;

        /**
         * The expected mod count.
         */
        int expectedRemoveCount;

        /**
         * Create a valid updating iterator object.
         */
        public UpdatingIterator(){
            current = null;
        }

        /**
         * Returns true if the iteration has more elements.
         * (In other words, returns true if next would return an element rather than throwing an exception.)
         *
         * @return true if the iterator has more elements.
         */
        public boolean hasNext() {
            expectedRemoveCount = removeCount;

            //before first case
            if (current == null) {
                return getTop() != null;
            }

            //empty tree
            else if (getTop() == null) {
                return false;
            }
            
            Cell c = splay(getTop(), current);
            //c.key > current
            if (c.key().compareTo(current) > 0) {
                return true;
            }

            //falls under both cases. == && <
            return c.rt != null;
        }


        /**
         * Returns the next element in the iteration.
         * 
         * @return the next String in the iteration.
         * @throws java.util.ConcurrentModificationException thrown if remove has been called on the SplayFC tree since the most recent call to hasNext
         * @throws java.util.NoSuchElementException Iteration has no more elements.
         */
        public String next() throws java.util.ConcurrentModificationException, java.util.NoSuchElementException {
            if (expectedRemoveCount != removeCount)
            {
                throw new java.util.ConcurrentModificationException ();
            }
            if (hasNext()) {
                Cell c;

                //before first case
                if (current == null) {
                    current = splay(getTop(), STRING_MIN).key();
                    return current;
                }

                c = splay(getTop(), current);
                int cmp = c.key().compareTo(current);

                if(cmp > 0) {
                    current = c.key();
                    return current;
                }

                current = splay(c.rt, STRING_MIN).key();
                return current;
            }
            else {
                //When there is no next element
                //Is not concurrent because this is not due to remove.
                throw new java.util.NoSuchElementException();
            }
        }

        /**
         * Removes from the underlying collection the last element returned by the iterator.
         */
        public void remove() {
            ++removeCount;

            if (current != null)  {
                SplayFC.this.remove(current);
            }                              
        }
    }
}
